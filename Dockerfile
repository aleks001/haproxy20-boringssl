FROM debian:latest

# take a look at http://www.lua.org/download.html for
# newer version

ENV HAPROXY_MAJOR=2.0 \
    HAPROXY_VERSION=2.0.0 \
    HAPROXY_SHA256=fe0a0d69e1091066a91b8d39199c19af8748e0e872961c6fc43c91ec7a28ff20  \
    LUA_VERSION=5.3.5 \
    LUA_URL=https://www.lua.org/ftp/lua-5.3.5.tar.gz \
    LUA_SHA1=112eb10ff04d1b4c9898e121d6bdf54a81482447 \
    BOR_URL=https://boringssl.googlesource.com/boringssl

RUN set -x \
  && apt -y update \
  && export buildDeps='python3 libpcre2-dev pkg-config git libpcre3-dev libreadline-dev gcc make zlib1g-dev cmake golang build-essential readline-common' \
  && apt -y install --no-install-recommends ca-certificates libpcre2-posix0 zlib1g curl iproute tar strace libreadline7 libpcre3 ${buildDeps} \
  && mkdir -p /usr/src/lua /usr/src/haproxy /usr/local/boringssl/lib \
  && git clone ${BOR_URL} /usr/src/boringssl \
  && mkdir /usr/src/boringssl/build \
  && cd /usr/src/boringssl/build \
  && cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo .. \
  && make -j "$(getconf _NPROCESSORS_ONLN)" \
  && cp -r ../include /usr/local/boringssl \
  && cp ssl/libssl.a crypto/libcrypto.a /usr/local/boringssl/lib \
  && cd /usr/src/ \
  && git clone https://github.com/vtest/VTest.git \
  && cd VTest \
  && make vtest \
  && cd /usr/src \
  && curl -sSLO ${LUA_URL} \
  && echo "${LUA_SHA1} lua-${LUA_VERSION}.tar.gz" | sha1sum -c - \
  && tar -xzf lua-${LUA_VERSION}.tar.gz -C /usr/src/lua --strip-components=1 \
  && make -C /usr/src/lua linux test install \
  && git clone http://git.haproxy.org/git/haproxy.git/ /usr/src/haproxy \
  && make -C /usr/src/haproxy  \
       TARGET=linux-glibc \
       USE_OPENSSL=1 \
       SSL_INC=/usr/local/boringssl/include \
       SSL_LIB=/usr/local/boringssl/lib \
       USE_PCRE_JIT=1 \
       USE_LUA=1 \
       USE_PTHREAD_PSHARED=1 \
       USE_REGPARM=1 \
       EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o" \
       all \
       install-bin \
  && mkdir -p /usr/local/etc/haproxy \
  && mkdir -p /usr/local/etc/haproxy/ssl \
  && mkdir -p /usr/local/etc/haproxy/ssl/cas \
  && mkdir -p /usr/local/etc/haproxy/ssl/crts \
  && cp -R /usr/src/haproxy/examples/errorfiles /usr/local/etc/haproxy/errors \
  && make -C /usr/src/haproxy/contrib/spoa_server/ install USE_LUA=1 \
  && cd /usr/src/haproxy \
  && VTEST_PROGRAM=/usr/src/VTest/vtest HAPROXY_PROGRAM=/usr/local/sbin/haproxy \
      make reg-tests \
   ; egrep -r ^ /tmp/haregtests*/* \
  && rm -rf /usr/src \
  && apt-get purge -y --auto-remove ca-certificates $buildDeps \
  && rm -rf /var/lib/apt/lists/* \
  && /usr/local/sbin/haproxy -vv \
  && /usr/local/bin/spoa -h

#  add this part back when 2.0 is released
#  && curl -sSLO http://www.haproxy.org/download/${HAPROXY_MAJOR}/src/haproxy-${HAPROXY_VERSION}.tar.gz \
#  && echo "${HAPROXY_SHA256} haproxy-${HAPROXY_VERSION}.tar.gz" | sha256sum -c \
#  && tar -xzf haproxy-${HAPROXY_VERSION}.tar.gz  -C /usr/src/haproxy --strip-components=1 \

COPY containerfiles /

#RUN chmod 555 /container-entrypoint.sh

EXPOSE 13443

ENTRYPOINT ["/container-entrypoint.sh"]

#CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.conf"]
#CMD ["haproxy", "-vv"]
